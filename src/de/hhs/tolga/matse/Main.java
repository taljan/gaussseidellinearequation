package de.hhs.tolga.matse;

import de.hhs.tolga.matse.algorithm.GaussSeidel;
import de.hhs.tolga.matse.data.LinearEquation;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here
        LinearEquation equation1 = LinearEquation.Builder.aLinearEquation(4, -1, 1, 7).build();
        LinearEquation equation2 = LinearEquation.Builder.aLinearEquation(4, 8, 1, -21).build();
        LinearEquation equation3 = LinearEquation.Builder.aLinearEquation(-2, 1, 5, 15).build();

        List<LinearEquation> equations = new ArrayList<>();
        equations.add(equation1);
        equations.add(equation2);
        equations.add(equation3);

        GaussSeidel.solveLinearEquation(equations, 0);
    }

}

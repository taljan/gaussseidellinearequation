package de.hhs.tolga.matse.algorithm;

import de.hhs.tolga.matse.data.LinearEquation;

import java.util.List;

/**
 * Helper comment:
 * x = (7 + y + z) / 4
 * y = (21 + 4x + z) / 8
 * z = (15 + 2x - y) / 15
 */

public final class GaussSeidel {

    private GaussSeidel() {}

    public static double[] solveLinearEquation(List<LinearEquation> equation, int startNumber) {
        boolean done = false;

        double calculatedY = 0.0;
        double calculatedX = 0.0;
        double calculatedZ = 0.0;

        while (!done) {
            for (int i = startNumber; i < equation.size(); i++) {
                LinearEquation currentEquation = equation.get(i);

                if (i == 0) {
                    double result = (currentEquation.getE() + currentEquation.getCoeffy() + currentEquation.getCoeffz()) / currentEquation.getCoeffx();
                    currentEquation.setX(
                            (currentEquation.getE() / currentEquation.getCoeffx()) +
                            (currentEquation.getCoeffy() / currentEquation.getCoeffx()) +
                            (currentEquation.getCoeffz() / currentEquation.getCoeffx())
                    );
                }
                if (i == 1) {
                    currentEquation.setY((currentEquation.getE() + currentEquation.getCoeffx() + currentEquation.getCoeffz()) / currentEquation.getCoeffy());
                }
                if (i == equation.size()) {
                    currentEquation.setZ((currentEquation.getE() + currentEquation.getX() + currentEquation.getCoeffy()) / currentEquation.getCoeffz());
                }
            }

            //Update old equation with new variable values.
            equation.get(0).setY(calculatedY);
            equation.get(0).setZ(calculatedZ);

            equation.get(1).setY(calculatedY);
            equation.get(2).setZ(calculatedZ);
        }

        return null;
    }

}

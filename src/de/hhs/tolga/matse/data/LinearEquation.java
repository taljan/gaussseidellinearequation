package de.hhs.tolga.matse.data;

public class LinearEquation {

    private double coeffx;
    private double coeffy;
    private double coeffz;

    private double x;
    private double y;
    private double z;
    private double e;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getE() {
        return e;
    }

    public void setE(double e) {
        this.e = e;
    }

    public double getCoeffx() {
        return coeffx;
    }

    public void setCoeffx(double coeffx) {
        this.coeffx = coeffx;
    }

    public double getCoeffy() {
        return coeffy;
    }

    public void setCoeffy(double coeffy) {
        this.coeffy = coeffy;
    }

    public double getCoeffz() {
        return coeffz;
    }

    public void setCoeffz(double coeffz) {
        this.coeffz = coeffz;
    }


    public static final class Builder {
        private double coeffx;
        private double coeffy;
        private double coeffz;
        private double e;

        private Builder(double coeffx, double coeffy, double coeffz, double e) {
            this.coeffx = coeffx;
            this.coeffy = coeffy;
            this.coeffz = coeffz;
            this.e = e;
        }

        public static Builder aLinearEquation(double coeffx, double coeffy, double coeffz, double e) {
            return new Builder(coeffx, coeffy, coeffz, e);
        }

        public Builder withCoeffx(double coeffx) {
            this.coeffx = coeffx;
            return this;
        }

        public Builder withCoeffy(double coeffy) {
            this.coeffy = coeffy;
            return this;
        }

        public Builder withCoeffz(double coeffz) {
            this.coeffz = coeffz;
            return this;
        }

        public LinearEquation build() {
            LinearEquation linearEquation = new LinearEquation();

            linearEquation.setX(1.0);
            linearEquation.setY(1.0);
            linearEquation.setZ(1.0);

            linearEquation.setCoeffx(coeffx);
            linearEquation.setCoeffy(coeffy);
            linearEquation.setCoeffz(coeffz);
            linearEquation.setE(e);

            return linearEquation;
        }
    }
}
